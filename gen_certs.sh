#!/usr/bin/env sh

mkdir -p certs/

docker run -it \
  -v $(pwd)/conf/openssl/openssl.conf:/openssl.conf:ro \
  -v $(pwd)/certs/:/certs/ \
  frapsoft/openssl \
  req -x509 \
    -newkey rsa -sha256 -days 3650 -nodes \
    -keyout /certs/pwdmgr.key -out /certs/pwdmgr.crt \
    -config /openssl.conf -extensions ext
